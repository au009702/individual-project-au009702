<?php
//Variables holding the server name, database username, database password, and database name
$serverName = "localhost";
$dBUsername = "root";
$dBPassword = "";
$dBName = "phpproject01";
//Creates a connection to the database
$conn = mysqli_connect($serverName, $dBUsername, $dBPassword, $dBName); 
//Checking if the connection was successful
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}