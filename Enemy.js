class Enemy {
  /*Constructor function that initializes the enemy class
  with the following parameters*/
 constructor(x, y, radius, color, velocity) {
  //the coordinates of the enemy
  this.x = x
  this.y = y
  //radius (health) of the enemy
  this.radius = radius 
  this.color = color 
  this.velocity = velocity
  //default type of the enemy is Linear
  this.type = 'Linear'
  //Image object for different appereances
  this.image = new Image()
  this.image.src = './images/meteor.png'
  this.image2 = new Image()
  this.image2.src = './images/ship_1.png'
  this.image3 = new Image()
  this.image3.src = './images/ship_2.png'
  //angle for direction of the enemy
  this.radians = 0
  //center position of the enemy
  this.center = {
    x,
    y
  }


  //math.random produces a number between 0 - 1
  let randomNumber = Math.random();
  //creation of the other types of enemies
    if (randomNumber < 0.2) {
      this.type = 'Homing';
    } else if (randomNumber < 0.4) {
      this.type = 'Spinning';
    } else {
      this.type = 'Homing Spinning';
    }
    

 }
//draw method of object 'image'
draw() {
//save the current context state
ctx.save()
//set the global alpha value
ctx.globalAlpha = this.alpha
//translate the canvas context to the center of the object's image
ctx.translate(this.x + this.image.width / 2, this.y + this.image.height / 2)
//rotate the context based on the object's radians value
ctx.rotate(this.radians)
//translate back to original
ctx.translate(-this.x - this.image.width / 2, -this.y - this.image.height / 2)
//draw the object at the current position
ctx.drawImage(this.image, this.x , this.y)
//restore the previous context state
ctx.restore()
}

  draw2() {
   ctx.save()
 ctx.globalAlpha = this.alpha

 ctx.translate(this.x + this.image2.width / 2, this.y + this.image2.height / 2)
 ctx.rotate(this.radians)
ctx.translate(-this.x - this.image2.width / 2, -this.y - this.image2.height / 2)

 ctx.drawImage(this.image2, this.x , this.y)
 ctx.restore()
 }

  draw3() {
   ctx.save()
 ctx.globalAlpha = this.alpha

 ctx.translate(this.x + this.image3.width / 2, this.y + this.image3.height / 2)
 ctx.rotate(this.radians)
ctx.translate(-this.x - this.image3.width / 2, -this.y - this.image3.height / 2)

 ctx.drawImage(this.image3, this.x , this.y)
 ctx.restore()
 }

 update() {
   

  if (this.type === 'Spinning') {
      this.draw3()
      

  //spinning enemies
  this.radians += 0.1

  this.center.x += this.velocity.x 
  this.center.y += this.velocity.y

  this.x =  this.center.x + Math.cos(this.radians) * 3
  this.y =  this.center.y + Math.sin(this.radians) * 3 }

  
  //check if enemy type is 'Homing'
  else if (this.type === 'Homing') {
  //call the 'draw2()' method to draw its image
  this.draw2()
  //calculate the angle between the players position and the enemy's position
  const angle = Math.atan2(player.position.y - this.y, player.position.x - this.x) 
  //update the velocity based on the calculated angle
  this.velocity.x = Math.cos(angle) * 3
  this.velocity.y = Math.sin(angle) * 3
  } 

  else if (this.type === 'Homing Spinning') {
  this.draw2()
  this.radians += 0.1

  const angle = Math.atan2(player.position.y - this.center.y, player.position.x - this.center.x)

  this.velocity.x = Math.cos(angle) * 3
  this.velocity.y = Math.sin(angle) * 3

  this.center.x += this.velocity.x 
  this.center.y += this.velocity.y

  this.x =  this.center.x + Math.cos(this.radians) * 3
  this.y =  this.center.y + Math.sin(this.radians) * 3 
    
  }
  else if (this.type === 'Linear'){
    this.draw()
   }
    this.x = this.x + this.velocity.x 
    this.y = this.y + this.velocity.y
  
  
    
 }
}

function spawnEnemies() {
//repeatedly execute this code block
 setInterval(() => {
//random health variable
const health = Math.random() * (30 - 20) + 20
      
let x
let y
//Spawn location either left or right side or top or bottom of the screen
if (Math.random() < 0.5){
x =  Math.random() < 0.5 ? 0 - health: canvas.width + health
y = Math.random() * canvas.height
} else {
x = Math.random() * canvas. width
y = Math.random() < 0.5 ? 0 - health: canvas.height + health
}
   
   //Getting a random color for the enemies
   const creating = 1 

   //Making the 'linear' enemies go towards the center
   const angle = Math.atan2(canvas.height / 2 - y, canvas.width / 2 -  x)
   const velocity = {
  x: Math.cos(angle) * 3,
  y: Math.sin(angle) * 3
 }

//Pusing new enemies into the array every 0.5 seconds 
enemies.push(new Enemy(x, y, health, creating, velocity))
}, 500)
}

